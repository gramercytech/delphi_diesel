var slideData = {
	1 : {
		title: "", 
		content: {
			images: [ 
				{
					fileName: "new/Header", 
					class: "full-width", 
					css: {
						width: "35%",
						top: "7%", 
						left: "3%"
					}, 
					animation: { name: "fadeIn", delay: 0}
				},
				{
					fileName: "new/Title_01_02", 
					class: "col-xs-6 left", 
					css: {
						"z-index": -1,
						top: 180,
						left: 100,
						width: "51%"
					}, 
					animation: { name: "slideInLeft", delay: 0}
				}, 
				{
					fileName: "new/Title_01_03", 
					class: "col-xs-6 right",
					css: {
						"z-index": -1,
						width: "40%",
						top: 300, 
						right: 50
					}, 
					animation: { name: "slideInRight", delay: 0}
				}, 
				{
					fileName: "new/Title_01_06", 
					css: {
						left: 490,
						top: 700
					}, 
					animation: { name: "slideInRight", delay: 0}
				}, 
				{
					fileName: "new/Title_01_07", 
					css: {
						left: 640,
						top: 200
					}, 
					animation: { name: "slideInRight", delay: 0}
				}, 
				{
					fileName: "new/Title_01_08", 
					css: {
						left: 1110,
						top: 690
					}, 
					animation: { name: "slideInRight", delay: 0}
				}, 
				{
					fileName: "new/Title_01_09", 
					css: {
						left: 1390,
						top: 200
					}, 
					animation: { name: "slideInRight", delay: 0}
				}, 
				{
					fileName: "new/Background_Trap_1", 
					css: {
						width: "100%",
						top: "3%",
						left: 0, 
						"z-index": -1
					}, 
					animation: { name: "fadeIn", delay: 0}
				},
				{
					fileName: "new/logo-delphi@2x", 
					css: {
						width: "16%", 
						left: "4%", 
						top: "79%"
					}, 
					animation: { name: "zoomIn", delay: 150}
				}
			], 
			buttons: [
				{
					id: "fis_btn",
					class: "slide-btn",
					css: {
						width: "41%", 
						height: "10%",
						left: 0, 
						top: "7%"
					}
				},
				{
					id: "cvfi_btn", 
					class: "slide-btn",
					css: {
						width: "15%",
						height: "28%",
						top: "19%", 
						left: "35%"
					}
				},
				{
					id: "scrd_btn", 
					class: "slide-btn",
					css: {
						width: "15%",
						height: "28%",
						top: "71%", 
						left: "27%"
					}
				},
				{
					id: "clc_btn", 
					class: "slide-btn",
					css: {
						width: "15%",
						height: "28%",
						top: "71%", 
						left: "62%"
					}
				},
				{
					id: "lvfi_btn", 
					class: "slide-btn",
					css: {
						width: "15%",
						height: "28%",
						top: "20%", 
						left: "76%"
					}
				}

			]
		}, 
		footer: "" 
	},
	2 : {
		title: "Commercial Vehicle Market Drivers", 
		content: {
			images: [
				{
					fileName: "new/VP_01_01", 
					class: "", 
					css: {
						left: 0,
						top: 120,
						width: "49%"
					}
				}, 
				// {
				// 	fileName: "S2_1b@2x",
				// 	class: "", 
				// 	css: {
				// 		left: 118,
				// 		top: 290,
				// 		width: 701
				// 	}, 
				// 	animation: { name: "slideInLeft", delay: 100}
				// }, 
				{
					fileName: "new/VP_01_03",
					class: "", 
					css: {
						left: 25, 
						bottom: 0,
						width: "47%"

					}
				}, 
				// {
				// 	fileName: "S2_2b@2x",
				// 	class: "", 
				// 	css: {
				// 		left: 120, 
				// 		bottom: 147, 
				// 		width: 555
				// 	}, 
				// 	animation: { name: "slideInLeft", delay: 100}
				// },
				{
					fileName: "new/VP_01_02", 
					class: "", 
					css: {
						top: 124,
						left: 890,
						width: 855
					}
				}, 
				// {
				// 	fileName: "S2_3b@2x",
				// 	class: "", 
				// 	css: {
				// 		top: 229, 
				// 		width: 718, 
				// 		left: 997
				// 	}, 
				// 	animation: { name: "slideInUp", delay: 150}
				// }
			]
		}, 
		footer: "" 
	},
	3 : {
		title: "Light Vehicle Market Drivers", 
		content: {
			images: [
				{
					fileName: "new/VP_02_headerText",
					class: "col-xs-12",
					css: {
						left: 0, 
						width: 1750
					}, 
					animation: { name: "fadeIn", delay: 0}
				},
				{
					fileName: "new/VP_02_01",
					class: "col-xs-4",
					css: {
						left: 0,
						top: 234,
						width: 546
					}
				},
				// {
				// 	fileName: "S3_1b@2x",
				// 	class: "col-xs-4",
				// 	css: {
				// 		left: 64,
				// 		top: 286,
				// 		width: 486
				// 	}, 
				// 	animation: { name: "slideInLeft", delay: 0}
				// },
				{
					fileName: "new/VP_02_02",
					class: "col-xs-4",
					css: {
						left: 4,
						top: 490,
						width: 583
					}
				},
				// {
				// 	fileName: "S3_2b@2x",
				// 	class: "col-xs-4",
				// 	css: {
				// 		left: 63,
				// 		top: 527,
				// 		width: 491
				// 	}, 
				// 	animation: { name: "slideInLeft", delay: 0}
				// },
				{
					fileName: "S3_3@2x",
					class: "col-xs-4",
					css: {
						left: 571,
						top: 220
					}
				},
				{
					fileName: "new/VP_02_05",
					class: "col-xs-4",
					css: {
						left: 1153,
						top: 240, 
						width: 623
					},
					animation: {
						name: "fadeIn", 
						delay: 0
					}
				},
				// {
				// 	fileName: "S3_4b_1@2x",
				// 	class: "col-xs-4",
				// 	css: {
				// 		left: 1204,
				// 		top: 276,
				// 		width: 520
				// 	}, 
				// 	animation: { name: "slideInUp", delay: 0}
				// },
			]
		}, 
		footer: "Diesel is a key technology for Light Commercial Vehicle<br> but the market for Passenger Car is uncertain" 
	},
	4 : {
		title: "Delphi Technologies Diesel Injector Technology Convergence", 
		content: {
			images: [
				{
					fileName: "new/VP_03_01", 
					class: "",
					css: {
						left: 38, 
						top: 219
					}, 
					animation: {
						name: "fadeIn", 
						delay: 0
					}
				}, 
				// {
				// 	fileName: "S4_1b@2x",
				// 	class: "",
				// 	css: {
				// 		left: 148, 
				// 		top: 236, 
				// 		width: 1584
				// 	}, 
				// 	animation: { name: "slideInLeft", delay: 100}
				// }
			]
		}, 
		footer: "Common injector technology leveraging combined scale of<br>Commercial Vehicle & Light Vehicle" 
	},
	5 : {
		title: "Next Generation Fuel Injection Systems", 
		content: {
			images: [
				// {
				// 	fileName: "s15_02@2x", 
				// 	class: "col-xs-10",
				// 	css: {
				// 		left: 20, 
				// 		top: 139, 
				// 		width: 1380
				// 	},
				// 	animation: { name: "zoomIn", delay: 0}
				// },
				{
					fileName: "new/CV_05_01", 
					class: "col-xs-10",
					css: {
						left: 20, 
						top: 148,
						width: 1407
					},
					animation: { name: "fadeInLeft", delay: 0}
				},
				// {
				// 	fileName: "s15_04@2x", 
				// 	class: "col-xs-10",
				// 	css: {
				// 		right: 359, 
				// 		width: 1107, 
				// 		top: 229
				// 	},
				// 	animation: { name: "fadeInRight", delay: 0}
				// },
				{
					fileName: "new/CV_05_02", 
					class: "col-xs-2",
					css: {
						right: 35, 
						top: 235, 
						width: 276
					},
					animation: { name: "fadeInDown", delay: 0}
				},
				// {
				// 	fileName: "s15_06@2x", 
				// 	class: "col-xs-2",
				// 	css: {
				// 		right: 0, 
				// 		top: 324
				// 	},
				// 	animation: { name: "fadeInDown", delay: 0}
				// }
			]
		}, 
		footer: "Flexible approach: Providing anything from hardware only to<br>fully-integrated solutions" 
	},
	6 : {
		title: "Commercial Vehicle Fuel Injection System", 
		content: {
			images: [{
				fileName: "new/CV_01_01", 
				class: "", 
				css: {
					left: 40, 
					top: 128
				}, 
				animation: { name: "fadeIn", delay: 0}
			}]
		}, 
		footer: "The heart and brain of the modern commercial vehicle powertrain" 
	},
	7 : {
		title: "Current Generation Fuel Injection Systems", 
		content: {
			images: [
				{
					fileName: "new/CV_02_01",
					class: "col-xs-6 left", 
					css: {
						width: 880, 
						top: 161, 
						left: 43
					},
					animation: { name: "fadeIn", delay: 0}
				},
				{
					fileName: "new/CV_02_02",
					class: "col-xs-6 right", 
					css: {
						width: 790, 
						top: 164
					},
					animation: { name: "fadeIn", delay: 0}
				}
			]
		}, 
		footer: "Leveraging common technology to extend the life of customer platforms" 
	},
	8 : {
		title: "Next Generation Fuel Injection Systems", 
		content: {
			images: [
				{
					fileName: "new/CV_03_01",
					class: "col-xs-6 left", 
					css: {
						width: 776, 
						top: 161, 
						left: 103
					},
					animation: { name: "fadeIn", delay: 0}
				},
				{
					fileName: "new/CV_03_02@2x",
					class: "col-xs-6 right", 
					css: {
						width: 790, 
						top: 164
					},
					animation: { name: "fadeIn", delay: 0}
				}
			]
		}, 
		footer: "Miniaturized modular technology for future CV Powertrains" 
	},
	9 : {
		title: "Next Generation Fuel Injection Systems", 
		content: {
			images: [
				{
					fileName: "new/LV_04_01",
					class: "col-xs-6 left", 
					css: {
						left: 22, 
						top: 150, 
						width: 908
					}, 
					animation: { name: "slideInLeft", delay: 0}
				},
				{
					fileName: "new/LV_05_02", 
					class: "col-xs-6 right", 
					css: {
						right: 53,
						width: 676, 
						top: 177
					}, 
					animation: { name: "slideInLeft", delay: 0}
				}
			]
		}, 
		footer: "Common controller hardware and software, scalable for all Powertrains" 
	},
	10 : {
		title: "Next Generation Fuel Injection Systems", 
		content: {
			images: [
				{
					fileName: "new/CV_05_01", 
					class: "col-xs-10",
					css: {
						left: 20, 
						top: 148,
						width: 1407
					},
					animation: { name: "fadeInLeft", delay: 0}
				},
				{
					fileName: "new/CV_05_02", 
					class: "col-xs-2",
					css: {
						right: 35, 
						top: 235, 
						width: 276
					},
					animation: { name: "fadeInDown", delay: 0}
				},
			]
		}, 
		footer: "Flexible approach: Providing anything from hardware only to<br>fully-integrated solutions" 
	},
	11 : {
		title: "Light Vehicle Fuel Injection System", 
		content: {
			images: [
				{
					fileName: "new/LV_01_01",
					class: "col-xs-6 left", 
					css: {
						left: 42, 
						top: 150, 
						width: 868
					}, 
					animation: { name: "slideInLeft", delay: 0}
				},
				{
					fileName: "new/LV_01_02", 
					class: "col-xs-6 right", 
					css: {
						right: 53,
						width: 698, 
						top: 157
					}, 
					animation: { name: "slideInLeft", delay: 0}
				}
			]
		}, 
		footer: "" 
	},
	12 : {
		title: "Current Generation Fuel Injection Systems", 
		content: {
			images: [
				{
					fileName: "new/LV_02_01",
					class: "col-xs-6 left", 
					css: {
						left: 100, 
						top: 75, 
						width: 760
					}, 
					animation: { name: "slideInLeft", delay: 0}
				},
				{
					fileName: "new/LV_02_02@2x", 
					class: "col-xs-6 right", 
					css: {
						right: 63,
						width: 828, 
						top: 187
					}, 
					animation: { name: "slideInLeft", delay: 0}
				}
			]
		}, 
		footer: "" 
	},
	13 : {
		title: "Next Generation Fuel Injection Systems", 
		content: {
			images: [
				{
					fileName: "new/LV_03_01",
					class: "col-xs-6 left", 
					css: {
						left: 20, 
						top: 125, 
						width: 840
					}, 
					animation: { name: "slideInLeft", delay: 0}
				},
				{
					fileName: "new/LV_03_03@2x", 
					class: "col-xs-6 right", 
					css: {
						right: 33,
						width: 857, 
						top: 150
					}, 
					animation: { name: "slideInLeft", delay: 0}
				}
			]
		}, 
		footer: "" 
	},
	14 : {
		title: "Next Generation Fuel Injection Systems", 
		content: {
			images: [
				{
					fileName: "new/CV_04_01@2x",
					class: "col-xs-6 left", 
					css: {
						left: 22, 
						top: 150, 
						width: 908
					}, 
					animation: { name: "slideInLeft", delay: 0}
				},
				{
					fileName: "new/LV_05_02", 
					class: "col-xs-6 right", 
					css: {
						right: 53,
						width: 676, 
						top: 177
					}, 
					animation: { name: "slideInLeft", delay: 0}
				}
			]
		}, 
		footer: "Common controller hardware and software, scalable for all Powertrains" 
	},
	15 : {
		title: "Next Generation Fuel Injection Systems", 
		content: {
			images: [
				{
					fileName: "new/CV_05_01", 
					class: "col-xs-10",
					css: {
						left: 20, 
						top: 148,
						width: 1407
					},
					animation: { name: "fadeInLeft", delay: 0}
				},
				{
					fileName: "new/CV_05_02", 
					class: "col-xs-2",
					css: {
						right: 35, 
						top: 235, 
						width: 276
					},
					animation: { name: "fadeInDown", delay: 0}
				}
			]
		}, 
		footer: "Flexible approach: Providing anything from hardware only to<br>fully-integrated solutions" 
	},
	16 : {
		title: "Selective Catalytic Reduction", 
		content: {
			images: [
				{
					fileName: "new/SCR_01_01",
					class: "left", 
					css: {
						top: 108, 
						width: 1020, 
						left: 23
					}, 
					animation: { name: "fadeInLeft", delay: 0}
				},
				{
					fileName: "S16_03@2x",
					class: "right", 
					css: {
						top: 84, 
						left: 511
					}, 
					animation: { name: "fadeInRight", delay: 0}
				}		
			]
		}, 
		footer: "Leveraging fuel-injection and controls expertise to reduce emissions" 
	},
	17 : {
		title: "Selective Catalytic Reduction", 
		content: {
			images: [
				{
					fileName: "new/SCR_02_01", 
					class: "",
					css: {
						width: 1030,
						left: 32
					},
					animation: {
						name: "fadeIn",
						delay: 0
					}
				},
				{
					fileName: "new/SCR_02_02", 
					class: "",
					css: {
						width: 550, 
						top: 200, 
						left: 0
					},
					animation: {
						name: "slideInDown",
						delay: 100
					}
				},
				{
					fileName: "new/SCR_02_03", 
					class: "",
					css: {
						width: 550, 
						top: 213, 
						left: 610
					},
					animation: {
						name: "slideInDown",
						delay: 200
					}
				},
				{
					fileName: "new/SCR_02_04", 
					class: "",
					css: {
						width: 550, 
						top: 283,
						left: 1184
					},
					animation: {
						name: "slideInDown",
						delay: 300
					}
				},
				// {
				// 	fileName: "s16_03_caption2x",
				// 	class: "", 
				// 	css: {
				// 		width: "30%",
				// 		top: "73%",
				// 		left: "3%"
				// 	},
				// 	animation: {
				// 		name: "slideInUp",
				// 		delay: 320
				// 	}
				// },
				// {
				// 	fileName: "s16_04_caption2x",
				// 	class: "", 
				// 	css: {
				// 		width: "30%",
				// 		top: "73%",
				// 		left: "36%"
				// 	},
				// 	animation: {
				// 		name: "slideInUp",
				// 		delay: 320
				// 	}
				// },
				// {
				// 	fileName: "s16_05_caption2x",
				// 	class: "", 
				// 	css: {
				// 		width: "30%",
				// 		top: "73%",
				// 		left: "68%"
				// 	},
				// 	animation: {
				// 		name: "slideInUp",
				// 		delay: 320
				// 	}
				// }	
			]
		}, 
		footer: "Leveraging fuel-injection and controls expertise to reduce emissions" 
	},
	18 : {
		title: "Closed Loop Control Fuel Injection System", 
		content: {
			images: [
				{
					fileName: "new/CLC_01_01_text",
					class: "col-xs-6 left", 
					css: {
						left: 25, 
						top: 130, 
						width: 790
					}, 
					animation: { name: "slideInLeft", delay: 0}
				},
				{
					fileName: "new/CLC_01_01@2x", 
					class: "col-xs-6 right", 
					css: {
						width: 810, 
						top: 120,
						right: 141
					}, 
					animation: { name: "zoomIn", delay: 0}
				},
				// {
				// 	fileName: "S18_2b", 
				// 	class: "col-xs-6 right", 
				// 	css: {
				// 		width: 550, 
				// 		top: 122,
				// 		right: 156, 
				// 		overflow: "visible"
				// 	}, 
				// 	animation: { name: "rotateIn", delay: 0}
				// }
			]
		}, 
		footer: "Injector on-board electronics close the loop efficiently" 
	},
	19 : {
		title: "Commercial Vehicle Market Drivers", 
		content: {
			images: [
				{
					fileName: "new/VP_01_01", 
					class: "", 
					css: {
						left: 0,
						top: 120,
						width: "49%"
					}
				}, 
				{
					fileName: "new/VP_01_03",
					class: "", 
					css: {
						left: 25, 
						bottom: 0,
						width: "47%"

					}
				}, 
				{
					fileName: "new/VP_01_02", 
					class: "", 
					css: {
						top: 124,
						left: 890,
						width: 855
					}
				}, 
			]
		}, 
		footer: "" 
	},
	20 : {
		title: "Light Vehicle Market Drivers", 
		content: {
			images: [
				{
					fileName: "new/VP_02_headerText",
					class: "col-xs-12",
					css: {
						left: 0, 
						width: 1750
					}, 
					animation: { name: "fadeIn", delay: 0}
				},
				{
					fileName: "new/VP_02_01",
					class: "col-xs-4",
					css: {
						left: 0,
						top: 234,
						width: 546
					}
				},
				{
					fileName: "new/VP_02_02",
					class: "col-xs-4",
					css: {
						left: 4,
						top: 490,
						width: 583
					}
				},
				{
					fileName: "S3_3@2x",
					class: "col-xs-4",
					css: {
						left: 571,
						top: 220
					}
				},
				{
					fileName: "new/VP_02_05",
					class: "col-xs-4",
					css: {
						left: 1153,
						top: 240, 
						width: 623
					},
					animation: {
						name: "fadeIn", 
						delay: 0
					}
				},
			]
		}, 
		footer: "Diesel is a key technology for Light Commercial Vehicle<br> but the market for Passenger Car is uncertain" 
	},
	21 : {
		title: "LV Diesel Injector Range", 
		content: {
			images: [
				{
					fileName: "new/CLC_02_01", 
					class: "left",
					css: {
						width: 725, 
						left: 30
					},
					animation: { name: "slideInDown", delay: 100 }
				},
				{
					fileName: "new/CLC_02_02", 
					class: "right",
					css: {
						width: 725, 
						top: 135
					},
					animation: { name: "slideInDown", delay: 200 }
				}
			]
		}, 
		footer: "" 
	},
	22 : {
		title: "LV Closed Loop Injector Control", 
		content: {
			images: [
				{
					fileName: "new/CLC_03_01",
					class: "left",
					css: {
						width: 924,
   						left: 30
					},
					animation: { name: "slideInDown", delay: 100}
				},
				{
					fileName: "new/CLC_03_02",
					class: "right",
					css: {
						width: 425, 
						top: 110, 
						right: 477
					},
					animation: { name: "slideInLeft", delay: 200}
				},
				{
					fileName: "new/CLC_03_03",
					class: "right",
					css: {
						width: 418,
						right: 12
					},
					animation: { name: "slideInRight", delay: 200}
				}
			]
		}, 
		footer: "ICL with switch technology presents an excellent price-performance" 
	},
	23 : {
		title: "CV Diesel Injector Range", 
		content: {
			images: [
				{
					fileName: "new/CLC_04_01",
					class: "left",
					css: {
						width: 870,
						left: 22
					},
					animation: { name: "fadeInLeft", delay: 150}
				},
				{
					fileName: "new/CLC_04_02",
					class: "right",
					css: {
						width: 850, 
						top: 115
					},
					animation: { name: "fadeInDown", delay: 250}
				}
			]
		}, 
		footer: "" 
	},
	24 : {
		title: "CV Closed Loop Injector Control", 
		content: {
			images: [
				{
					fileName: "new/CLC_05_01",
					class: "left",
					css: {
						width: 750,
						left: 30
					},
					animation: { name: "fadeInLeft", delay: 150}
				},
				{
					fileName: "new/CLC_05_02",
					class: "right",
					css: {
						width: 930, 
						right: 50
					},
					animation: { name: "zoomIn", delay: 150}
				},
				// {
				// 	fileName: "s24_2b@2x",
				// 	class: "right",
				// 	css: {
				// 		width: 520,
				// 		right: 260, 
				// 		top: 200, 
				// 		overflow: "visible"
				// 	},
				// 	animation: { name: "rotateIn", delay: 200}
				// }
			]
		}, 
		footer: "Injector on-board electronics close the loop efficiently" 
	},
	25 : {
		title: "DFI 21 Common Rail Injectors", 
		content: {
			images: [
				{
					fileName: "new/CLC_06_01",
					class: "left", 
					css: {
						width: 940, 
						top: 130, 
						left: 30
					},
					animation: { name: "fadeInLeft", delay: 0}
				},
				{
					fileName: "new/CLC_06_02",
					class: "right", 
					css: {
						width: 700, 
						top: 130,
					},
					animation: { name: "fadeInLeft", delay: 100}
				},
				// {
				// 	fileName: "s22_04@2x",
				// 	class: "left", 
				// 	css: {
				// 		width: 1000, 
				// 		top: 530,
				// 		left: 30
				// 	},
				// 	animation: { name: "fadeInLeft", delay: 200}
				// },
				// {
				// 	fileName: "s22_05@2x",
				// 	class: "right",
				// 	css: {
				// 		width: 700
				// 	},
				// 	animation: { name: "slideInLeft", delay: 250}
				// }
			]
		}, 
		footer: "Flexible injector architectures with common injector technology" 
	},
	26 : {
		title: "Westport LNG High Pressure Direct Injection (HPDI)", 
		content: {
			images: [
				{
					fileName: "new/CLC_07_01",
					class: "left",
					css: {
						width: 840, 
						left: 25
					},
					animation: { name: "zoomInLeft", delay: 0}
				},
				{
					fileName: "new/CLC_07_02",
					class: "right",
					css: {
						width: 844
					},
					animation: { name: "zoomInRight", delay: 0}
				}
			]
		}, 
		footer: "Flexible injector architectures with common injector technology" 
	},
	27 : {
		title: "Westport LNG HPDI – Delphi Technologies Value Contribution", 
		content: {
			images: [
				{
					fileName: "new/CLC_08_01",
					class: "col-xs-4",
					css: {
						left: 40,
						width: 500
					},
					animation: { name: "bounceInDown", delay: 100}
				},
				{
					fileName: "new/CLC_08_02",
					class: "col-xs-4",
					css: {
						left: 600, 
						width: 575
					},
					animation: { name: "bounceInDown", delay: 200}
				},
				{
					fileName: "new/CLC_08_03",
					class: "col-xs-4",
					css: {
						left: 1231, 
						width: 506
					},
					animation: { name: "bounceInDown", delay: 300}
				},
				{
					fileName: "new/CLC_08_04",
					class: "col-xs-4",
					css: {
						left: 60,
						width: 400, 
						top: 620
					},
					animation: { name: "bounceInDown", delay: 100}
				},
				{
					fileName: "new/CLC_08_05",
					class: "col-xs-4",
					css: {
						left: 629, 
						width: 500, 
						top: 670
					},
					animation: { name: "bounceInDown", delay: 200}
				},
				{
					fileName: "new/CLC_08_06",
					class: "col-xs-4",
					css: {
						left: 1178, 
						top: 530
					},
					animation: { name: "bounceInDown", delay: 300}
				}
			]
		}, 
		footer: "" 
	},
	28 : {
		title: "High Pressure CV Pumps", 
		content: {
			images: [
				{
					fileName: "new/PS_01_01",
					class: "left",
					css: {
						width: 564
					},
					animation: { name: "bounceInLeft", delay: 100}
				},
				{
					fileName: "new/PS_01_02",
					class: "",
					css: {
						width: 670,
						left: 548, 
						top: 220
					},
					animation: { name: "bounceInDown", delay: 150}
				},
				{
					fileName: "new/PS_01_03",
					class: "right",
					css: {
						width: 564
					},
					animation: { name: "bounceInRight", delay: 100}
				}
			]
		}, 
		footer: "Flexible high performance pump modules covering<br>a wide range of CV applications" 
	},
	29 : {
		title: "CV Diesel Pump Range", 
		content: {
			images: [
				{
					fileName: "new/PS_02_02",
					class: "col-xs-6 right", 
					css: {
						width: 870
					},
					animation: { name: "fadeIn", delay: 0}
				},
				{
					fileName: "new/PS_02_01",
					class: "col-xs-6 left", 
					css: {
						width: 809, 
						left: 16
					},
					animation: { name: "fadeIn", delay: 0}
				}			
			]
		}, 
		footer: "" 
	},
	30 : {
		title: "LV Diesel HP Pump Range", 
		content: {
			images: [
				{
					fileName: "new/PS_03_02@2x",
					class: "col-xs-6 right", 
					css: {
					    top: 120
					},
					animation: { name: "fadeIn", delay: 0}
				},
				{
					fileName: "new/PS_03_01",
					class: "col-xs-6 left",
					css: {
						left: 20
					},
					animation: { name: "fadeIn", delay: 0}
				}
			]
		}, 
		footer: "" 
	},
	31 : {
		title: "CV Powertrain Controllers", 
		content: {
			images: [
				{
					fileName: "new/LV_Header_Text",
					css: {
						width: 820,
						top: 110, 
						left: 500
					},
					animation: { name: "zoomIn", delay: 0}
				},
				{
					fileName: "new/CS_02_01",
					class: "left",
					css: {
						width: 433,
						top: 250,
						left: 100
					},
					animation: { name: "fadeInLeft", delay: 0}
				},
				// {
				// 	fileName: "S29_04@2x",
				// 	class: "right",
				// 	css: {
				// 		width: 400,
				// 		top: 270
				// 	},
				// 	animation: { name: "slideInRight", delay: 50}
				// },
				{
					fileName: "new/CS_01_03@2x",
					class: "right",
					css: {
						width: 990,
						top: 290, 
						right: 78
					},
					animation: { name: "zoomIn", delay: 150}
				},
				// {
				// 	fileName: "S28_04@2x",
				// 	class: "right",
				// 	css: {
				// 		width: 400,
				// 		right: 787,
				// 		top: 320
				// 	},
				// 	animation: { name: "slideInLeft", delay: 50}
				// },
				{
					fileName: "S3_0b@2x",
					class: "left",
					css: {
						width: 200,
						left: 460, 
						top: 164
					},
					animation: { name: "fadeInLeft", delay: 0}
				}
			]
		}, 
		footer: "Next generation truck controller leveraging<br>common controller components and architecture" 
	},
	32 : {
		title: "LV Controllers", 
		content: {
			images: [
				{
					fileName: "new/CV_Header_Text",
					css: {
						width: 770,
						top: 100, 
						left: 500
					},
					animation: { name: "zoomIn", delay: 0}
				},
				{
					fileName: "new/CS_02_01",
					class: "left",
					css: {
						width: 433,
						top: 250,
						left: 100
					},
					animation: { name: "fadeInLeft", delay: 0}
				},
				{
					fileName: "new/CS_02_02",
					class: "left",
					css: {
						width: 235,
						left: 416, 
						top: 160
					},
					animation: { name: "fadeInLeft", delay: 0}
				},
				// {
				// 	fileName: "S29_04@2x",
				// 	class: "right",
				// 	css: {
				// 		width: 400,
				// 		top: 270, 
				// 		right: 730
				// 	},
				// 	animation: { name: "zoomInDown", delay: 50}
				// },
				{
					fileName: "new/CS_02_03",
					class: "right",
					css: {
						width: 980,
						top: 260,
						right: 20
					},
					animation: { name: "zoomIn", delay: 100}
				}
			]
		}, 
		footer: "Next generation Light Vehicle controller leveraging<br>common controller components and architecture" 
	}
};
	