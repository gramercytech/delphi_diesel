var headerData = {
	last_header : [
		{
			title: "ADVANCED INJECTION SYSTEMS", 
			slides: [6, 11]
		}, 
		{
			title: "TODAY", 
			slides : [7, 12]
		}, 
		{
			title: "TOMORROW", 
			slides : [8, 9, 13, 14]
		}, 
		{
			title: "THE DELPHI TECHNOLOGIES DIFFERENCE", 
			slides : [10, 15]
		}		
	]
};