var navData = {
	"buttons" : [
		{
			id: "cv-btn",
			text: "COMMERCIAL VEHICLE FIS", 
			goTo: "slide-6"
		},
		{
			id: "lv-btn",
			text: "LIGHT VEHICLE FIS", 
			goTo: "slide-11"
		}, 
		{
			id: "scr-btn",
			text: "SCR DOSING", 
			goTo: "slide-16"
		}, 
		{
			id: "clc-btn",
			text: "CLOSED LOOP CONTROL", 
			goTo: "slide-18"
		}
	], 
	"additional" : [
		{
			id: "cvm-btn",
			text: "CV MARKET", 
			goTo: "slide-19"
		}, 
		{
			id: "lvm-btn",
			text: "LV MARKET",
			goTo: "slide-20"
		},
		{
			id: "inj-btn",
			text: "INJECTORS",
			goTo: "slide-21"
		}, 
		{
			id: "pump-btn",
			text: "PUMPS",
			goTo: "slide-28"
		}, 
		{
			id: "cont-btn",
			text: "CONTROLLERS",
			goTo: "slide-31"
		}
	]
}