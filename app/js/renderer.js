// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
var $ = require('jQuery');
// include jquery animate.css functionality
$.fn.extend({
    animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.show();
        this.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
        return this;
    }
});

var slides = $('#main-slides');
var fisSlides = $('#fis-container');
var addSlides = $('#additional-container');
var header = $('.top-header');

Object.keys(slideData).forEach(function(key) {
  var section = $("<section>", {id: "slide-" + key});
  var title = $("<h1>");
  var container = $("<div>");
  var fis = [1, 2, 3, 4, 5];
  var add =[19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32];

  var slideNumber = $("<div>", {id: "slideNumber"});
  slideNumber.text(key);

  var images = slideData[key].content.images;
  var buttons = slideData[key].content.buttons;

  // create image components-
  for (var i = images.length - 1; i >= 0; i--) {
  	var imgDiv = $('<div>', {class: "img-div"});
    var img = $('<img>');

    img.attr('src', "assets/" + images[i].fileName + ".png");

    if (images[i].class && images[i].class != "") {
      imgDiv.addClass(images[i].class);
    }

    // set css styles
    if (images[i].css) {
    	imgDiv.css(images[i].css);
    }

    // set animation attribute
    if (images[i].animation) {
    	imgDiv.data('animate-name', images[i].animation.name);
    	imgDiv.data('animate-delay', images[i].animation.delay);
    } else {
    	imgDiv.data('animate-name', "fadeIn");
    	imgDiv.data('animate-delay', 100);    	
    }

    imgDiv.append(img)
    container.append(imgDiv);
  }

  // create button components
  if (buttons) {
	  for (var i = buttons.length - 1; i >= 0; i--) {
	    var btn = $('<button>', {id: buttons[i].id});

	    if (buttons[i].class && buttons[i].class != "") {
	      btn.addClass(buttons[i].class);
	    }

	    // set css styles
	    if (buttons[i].css) {
	    	btn.css(buttons[i].css);
	    }

	    container.append(btn);
	  }
  }

  title.addClass("h1 main-title");
  title.text(slideData[key].title);

  container.addClass("content row");

  section.append(title);
  section.append(container);
  section.append(slideNumber);

  // append footer if exists
  if (slideData[key].footer && slideData[key].footer != "") {
    var footer = $("<div>");

    var topLine = $("<div>", {class: "top-footer-line"});
    var btmLine = $("<div>", {class: "btm-footer-line"});
    // check char length for line height adjustments
    if (slideData[key].footer.length > 80) {
    	var footerText = $("<p>", {class: "double"});      	
    } else {
    	var footerText = $("<p>", {class: "one-line"});    	
    }

    var topImg = $('<img>', {src: "assets/new/Footer_1.png"});
    var btmImg = $('<img>', {src: "assets/new/Footer_2.png"});

    topLine.append(topImg);
    btmLine.append(btmImg);

    footer.addClass("footer-text");
    footerText.html(slideData[key].footer);

    footer.append(topLine);
    footer.append(btmLine);
    footer.append(footerText);

    section.append(footer);
  }

  if (fis.includes(parseInt(key))) {
  	fisSlides.append(section);
  } else if (add.includes(parseInt(key))) {
  	addSlides.append(section);
  } else {
  	slides.append(section);
  }
});

Object.keys(headerData).forEach(function(key) {
	// if (key == "initial_header") {
		var $ul = $("<ul>", {id: key});
	// } else {
	// 	var $ul = $("<ul>", {id: key});
	// }

	var data = headerData[key];

	data.forEach(function(elem, i) {
		var $li = $("<li>");
		var title = elem.title;
		if (i == 0 && key == "initial_header") {
			$li.addClass('active');
		}

		$li.text( title );
		$ul.append($li);		
	})

	header.append($ul)
});

Object.keys(navData).forEach(function(key) {
	var footer = $('.footer-menu .navbar-nav');
	var dropUp = $('.dropdown-menu');
	var data = navData[key];

	data.forEach(function(elem, i) {
		if (key == "buttons") {
			var $li = $("<li>");
			var $btn = $("<button>", {class: 'btm-nav-btn', id: elem.id});

			$btn.text(elem.text);
			$li.append($btn);

			footer.append($li);	
		} else {
			var $li = $("<li>", {class: "additional-nav"});
			var $btn = $("<button>", {class: 'additional-nav', id: elem.id});

			$btn.text(elem.text);
			$li.append($btn);

			dropUp.append($li);
		}	
	});
});

const reveal = require('reveal');

reveal.initialize({
	controls: false,
	keyboard: true,
	touch: false,
	// default/cube/page/concave/zoom/linear/fade/none 
	transition: 'none',
	width: "90%",
	height: "80%",
	margin: "5%",
	dependencies: [
	 	// { src: '../node_modules/reveal/plugin/zoom-js/zoom.js', async: true }
		// { src: '../node_modules/reveal.js-menu/menu.js', async: true }
	]
})

$('#exit-fs').on('dblclick', function() {
	location.reload();
});

$('#home-btn').on('click', function() {
	reveal.slide(0, 0);
})
$('#cv-btn').on('click', function() {
	reveal.slide(1, 0);
})

$('.rightSlide').on('click', function() {
	if(!$(this).hasClass('disabled')) {
		if (reveal.getCurrentSlide().id == "slide-1") {
			reveal.right();
		} else {
			reveal.down();
		}
	}
})

$('.leftSlide').on('click', function() {
	if(!$(this).hasClass('disabled')) { 
		if (reveal.getCurrentSlide().id == "slide-6") {
			reveal.left();
		} else {
			reveal.up();
		}
	}
})

$('.btm-nav-btn').on('click', function() {
	var el = $(this);
	var sectionId = "";

	switch (el.attr('id')) {
		case "cv-btn":
			sectionId = "slide-6"
			break; 
		case "lv-btn":
			sectionId = "slide-11"
			break; 
		case "scr-btn": 
			sectionId = "slide-16"
			break; 
		case "clc-btn": 
			sectionId = "slide-18"
			break; 
		default: 
			break;
	}

	var indices = reveal.getIndices( document.getElementById(sectionId) );
	reveal.slide(indices.h, indices.v);
})

// FIRST SLIDE BUTTONS
$('.slide-btn').on('click', function() {
	var el = $(this);
	var sectionId = "";

	switch (el.attr('id')) {
		case "fis_btn": 
			sectionId = "slide-2"
			break;
		case "cvfi_btn":
			sectionId = "slide-6"
			break; 
		case "scrd_btn":
			sectionId = "slide-16"
			break; 
		case "clc_btn": 
			sectionId = "slide-18"
			break; 
		case "lvfi_btn": 
			sectionId = "slide-11"
			break; 
		default: 
			break;
	}

	var indices = reveal.getIndices( document.getElementById(sectionId) );
	reveal.slide(indices.h, indices.v);
})

// set close + navigate for additional popup
$('.additional-nav').on('click', function() {
	var btn = $(this);
	var slideId = reveal.getCurrentSlide().id;
	var slideNum = parseInt(slideId.split('-')[1]);

	if (slideNum > 0 && slideNum < 19) {
		$('#close').show();

		$('#close').on('click', function() {
			// only go to first slide if in FIS section
			if (slideNum < 5) {
				reveal.slide(0, 0);
			} else {
				var indices = reveal.getIndices( document.getElementById(slideId) );
				reveal.slide(indices.h, indices.v);
			}

			$('#close').hide();
		})
	}

	switch (btn.attr('id')) {
		case "cvm-btn": 
			sectionId = "slide-19"
			break;
		case "lvm-btn":
			sectionId = "slide-20"
			break; 
		case "inj-btn":
			sectionId = "slide-21"
			break; 
		case "pump-btn": 
			sectionId = "slide-28"
			break; 
		case "cont-btn": 
			sectionId = "slide-31"
			break; 
		default: 
			break;
	}

	var indices = reveal.getIndices( document.getElementById(sectionId) );
	reveal.slide(indices.h, indices.v);
})

function activateButton (name = null) {
	var buttons = $(".footer-menu .navbar-nav").children("li");
	var menuItems = $(".dropdown-menu").children();

	buttons.each(function() {
		if ($(this).children('button').attr('id') == name) {
			$(this).addClass('active');
		} else {
			$(this).removeClass('active');
		}
	})

	menuItems.each(function() {
		var item = $(this).children('button')[0];
		$(item).removeClass('active');
	})
};

function activateDropup (name) {
	var buttons = $(".footer-menu .navbar-nav").children("li");
	var menuItems = $(".dropdown-menu").children();

	buttons.each(function() {
		if ($(this).attr('id') == "dropUp") {
			$(this).addClass('active');
		} else {
			$(this).removeClass('active');
		}
	})

	menuItems.each(function() {
		var item = $(this).children('button')[0];
		if ($(item).attr('id') == name) {
			$(item).addClass('active');
		} else {
			$(item).removeClass('active');
		}
	})
};

function customHeader(index) {
	// $("#initial_header").hide();
	$("#last_header").hide();

	var custom = $("#custom_header");

	custom.children('li').each(function(i, el) {
		if (i == index) {
			$(el).addClass('active');
		} else {
			$(el).removeClass('active');
		}
	});

	custom.show();
};

function showHeader (type, index) {
	$("#last_header").show();
	$("#custom_header").hide();

	$("#last_header").children('li').each(function(i) {
		if (i == index) {
			$(this).addClass('active');
		} else {
			$(this).removeClass('active');
		}
	})
};

function hideImages(prevSlide) {
	var images = $(prevSlide).find('img');
	var topFooter = $('.top-footer-line');
	var btmFooter = $('.btm-footer-line');

	topFooter.removeAttr('style');
	btmFooter.removeAttr('style');
	
	$.each(images, function( index, value ) {
		$(value).removeAttr('style');
	});
};

reveal.addEventListener( 'ready', function(e) {
	var slideId = parseInt(e.currentSlide.id.split('-')[1]);

	slideEvent(slideId);
	animate(slideId);
});

reveal.addEventListener( 'slidechanged', function(e) {
	var slideId = parseInt(e.currentSlide.id.split('-')[1]);

	slideEvent(slideId);
	animate(slideId);

	hideImages(e.previousSlide);
}, false );

// animate slide
function animate(slideId) {
	var slide = $('#slide-' + slideId);
	var slideImages = slide.find('.content').children();
	var footer = slide.children('.footer-text');
	var title = slide.find('.main-title');

	title.animate({opacity: 1}, 1800);

	slideImages.each( function(index, element) {
		var animation = $(this).data('animate-name');
		var delay = $(this).data('animate-delay');

        if (animation) {
        	setTimeout(function() {
        		$(element).children().animateCss(animation);
        	}, delay)
        }
	})

	if (footer.length != 0) {
		var topLine = $(footer).children('.top-footer-line');
		var btmLine = $(footer).children('.btm-footer-line');

		topLine.animate({
			width: "100%"
		}, 1500);

		btmLine.animate({
			width: "100%"
		}, 1500);
	}

};

// trigger slide events
function slideEvent(slideId) {
	var aisSlides = headerData["last_header"][0].slides;
	var todaySlides = headerData["last_header"][1].slides;
	var tomorrowSlides = headerData["last_header"][2].slides;
	var diffSlides = headerData["last_header"][3].slides;

	// show close btn for FIS section
	if (slideId > 1 && slideId < 6) {
		$('#close').show();
		$('#close').on('click', function() {
			reveal.slide(0, 0);
		})
	} else if ((slideId == 1 || slideId > 5 && slideId < 19) && $('#close').is(":visible")) {
		$('#close').hide();
	}

	// disable left arrow
	if (reveal.isFirstSlide() || slideId == 19) {
		$('.leftSlide').addClass('disabled');
	} else {
		$('.leftSlide').removeClass('disabled');
	}

	// disable right arrow
	if (reveal.isLastSlide() || slideId == 5 || slideId == 18) {
		$('.rightSlide').addClass('disabled');
	} else {
		$('.rightSlide').removeClass('disabled');
	}

	// show first background
	if (slideId == 1) {
		$('#first-background').show();
	} else {
		$('#first-background').hide();
	}

	if (slideId > 18 && !$('#close').is(":visible")) {
		$('#close').show();

		$('#close').on('click', function() {
			reveal.slide(0, 0);
		})
	}

	// toggle footer button display
	switch(true) {
		case (slideId > 5 && slideId < 11):
			activateButton("cv-btn");
			break;
		case (slideId > 10 && slideId < 16):
			activateButton("lv-btn");
			break;
		case (slideId > 15 && slideId < 18):
			activateButton("scr-btn");
			break;
		case (slideId == 18): 
			activateButton("clc-btn");
			break;
		case (slideId == 19):
			activateDropup("cvm-btn");
			break;
		case (slideId == 20):
			activateDropup("lvm-btn");
			break;
		case (slideId > 20 && slideId < 28):
			activateDropup("inj-btn");
			break;
		case (slideId > 27 && slideId < 31):
			activateDropup("pump-btn");
			break;
		case (slideId > 30):
			activateDropup("cont-btn");
			break;
		default:
			activateButton();
			break;
	}

	// toggle header text display
	switch(true) {
		case (slideId == 1): 
	        $('#custom_header').hide();
	        $('#last_header').hide();
	        break;
	    case (slideId == 2):
	       	customHeader(0);
	        break;
	    case (slideId == 19): 
	   		customHeader(1);
	    	break;
	    case (slideId == 16 || slideId == 17): // SCR Dosing
	    	customHeader(6);
	    	break;
	    case (slideId == 18): 
	    	customHeader(6);
	    	break;
	    case (slideId == 20): 
	   		customHeader(2);
	    	break;
	    case (slideId == 21): 
	   		customHeader(3);
	    	break;
	    case (slideId == 28): 
	   		customHeader(4);
	    	break;
	    case (slideId == 31):
	   		customHeader(5);
	    	break;
	   	case (aisSlides.includes(slideId)):
	        showHeader("last_header", 0);
	        break;
	    case (todaySlides.includes(slideId)):
	        showHeader("last_header", 1);
	        break;
	    case (tomorrowSlides.includes(slideId)):
	        showHeader("last_header", 2);
	        break;
	    case (diffSlides.includes(slideId)):
	        showHeader("last_header", 3);
	        break;	        	        
	    default:
	    	break;
	}	
}
